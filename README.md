# Big Data Project

The goal of the project is to infer qualitative data regarding USA flights during the years 1994-2008. The data can be downloaded from http://stat-computing.org/dataexpo/2009/the-data.html.

Using both Hadoop and Spark provide the following information:

- ~~the percentage of canceled flights per day, throughout the entire data set~~
- ~~weekly percentages of delays that are due to weather, throughout the entire data set~~
- ~~the percentage of flights belonging to a given "distance group" that were able to halve their departure delays by the time they arrived at their destinations. Distance groups assort flights by their total distance in miles. Flights with distances that are less than 200 miles belong in group 1, flights with distances that are between 200 and 399 miles belong in group 2, flights with distances that are between 400 and 599 miles belong in group 3, and so on. The last group contains flights whose distances are between 2400 and 2599 miles.~~
- ~~a weekly "penalty" score for each airport that depends on both the its incoming and outgoing flights. The score adds 0.5 for each incoming flight that is more than 15 minutes late, and 1 for each outgoing flight that is more than 15 minutes late.~~  

**Also provide an additional data analysis defined by your group.**
Use charts to present the information that you have extracted from the data set.

**Name	Description**  

1	  Year	1987-2008  
2	  Month	1-12  
3	  DayofMonth	1-31  
4   DayOfWeek	1 (Monday) - 7 (Sunday)  
5	  DepTime	actual departure time (local, hhmm)  
6	  CRSDepTime	scheduled departure time (local, hhmm)  
7	  ArrTime	actual arrival time (local, hhmm)    
8	  CRSArrTime	scheduled arrival time (local, hhmm)  
9	  UniqueCarrier	unique carrier code  
10	FlightNum	flight number  
11	TailNum	plane tail number  
12	ActualElapsedTime	in minutes  
13	CRSElapsedTime	in minutes  
14	AirTime	in minutes   
15	ArrDelay	arrival delay, in minutes  
16	DepDelay	departure delay, in minutes  
17	Origin	origin IATA airport code  
18	Dest	destination IATA airport code  
19	Distance	in miles  
20	TaxiIn	taxi in time, in minutes  
21	TaxiOut	taxi out time in minutes  
22	Cancelled	was the flight cancelled?  
23	CancellationCode	reason for cancellation (A = carrier, B = weather, C = NAS, D = security)  
24	Diverted	1 = yes, 0 = no
25	CarrierDelay	in minutes   
26	WeatherDelay	in minutes  
27	NASDelay	in minutes  
28	SecurityDelay	in minutes  
29	LateAircraftDelay	in minutes  

# Notes

- The versions of Spark, python ... should be the same on the driver (your PC) and all the virtualized nodes
- Actually using: spark-2.3.0 / python-3.4.2
- When creating the Hadoop cluster, make sure to SSH into the name node and execute `hdfs dfs -put /hadoop/dfs/data-fs/*.csv /` to put the local files into the HDFS Cluster.
