from pyspark.sql.functions import *       # added for the function
from pyspark.sql import SparkSession
from pyspark.sql import Row

## To run if you want to test the connection
## to the Spark cluster

spark = SparkSession.builder \
    .master('spark://localhost:7077') \
    .appName('CancelledFlights') \
    .getOrCreate()
