#!/bin/bash

## Script to download the data files from 1994 to 2008
## Keeping it one year now for dev purposes

echo "###### Downloading ..."

for i in {1994..2008}
do
   wget "http://stat-computing.org/dataexpo/2009/$i.csv.bz2"
done

echo "###### Extracting the archives"

for i in {1994..2008}
do
   bzip2 -d "$i.csv.bz2"
done
