Day with highest Percentage WeatherDelays : 2008-51 with 13.11%
Day with lowest Percentage WeatherDelays  : 2004-39 with 0.52%

+-------+------------------+
|summary|        Percentage|
+-------+------------------+
|  count|               248|
|   mean| 3.555624185936193|
| stddev|1.9964216012297566|
|    min|0.5202106013886267|
|    max| 13.10816978548608|
+-------+------------------+